package com.training.financialanalyzer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.financialanalyzer.dto.ApiResponse;
import com.training.financialanalyzer.dto.PaymentDto;
import com.training.financialanalyzer.entity.Category;
import com.training.financialanalyzer.entity.Customer;
import com.training.financialanalyzer.entity.PaymentType;
import com.training.financialanalyzer.entity.Transaction;
import com.training.financialanalyzer.exception.CustomerNotFoundException;
import com.training.financialanalyzer.exception.InsufficientFundException;
import com.training.financialanalyzer.repository.CustomerRepository;
import com.training.financialanalyzer.repository.TransactionRepository;
import com.training.financialanalyzer.service.impl.MakePaymentServiceImpl;
import com.training.financialanalyzer.util.ResponseMessages;

@ExtendWith(SpringExtension.class)
class MakePaymentServiceImplTest {
	@InjectMocks
	private MakePaymentServiceImpl makePaymentServiceImpl;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private TransactionRepository transactionRepository;

	@Test
	void makePaymentUserIdNotFound() {
		Mockito.when(customerRepository.findById(1l)).thenReturn(Optional.empty());

		PaymentDto paymentDto = PaymentDto.builder().amount(22l).customerId(1l).category(Category.HOUSE_RENT)
				.paymentType(PaymentType.DEBIT).transactionDescription("Paying house rent").build();
		assertThrows(CustomerNotFoundException.class, () -> makePaymentServiceImpl.makePayment(paymentDto));
	}

	@Test
	void makePaymentInsficienFund() {
		Long customerId = 1L;
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customer.setOpeningBalance(20D);

		PaymentDto paymentDto = PaymentDto.builder().amount(222l).customerId(1l).category(Category.HOUSE_RENT)
				.paymentType(PaymentType.DEBIT).transactionDescription("Paying house rent").build();
		Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));
		assertThrows(InsufficientFundException.class, () -> makePaymentServiceImpl.makePayment(paymentDto));
	}

	@Test
	void makePaymentDebit() {
		Customer customer = Customer.builder().customerId(1l).openingBalance(20000l).build();
		Mockito.when(customerRepository.findById(1l)).thenReturn(Optional.of(customer));
		PaymentDto paymentDto = PaymentDto.builder().amount(2000l).customerId(1l).category(Category.HOUSE_RENT)
				.paymentType(PaymentType.DEBIT).transactionDescription("Paying house rent").build();

		ApiResponse payment = makePaymentServiceImpl.makePayment(paymentDto);
		assertEquals(ResponseMessages.TRANSACTION_COMPLETED_SUCCESSFULLY_MESSAGE, payment.message());

	}

	@Test
	void makePaymentCredit() {
		Customer customer = Customer.builder().customerId(1l).openingBalance(20000l).build();
		Mockito.when(customerRepository.findById(1l)).thenReturn(Optional.of(customer));
		PaymentDto paymentDto = PaymentDto.builder().amount(2000l).customerId(1l).category(Category.HOUSE_RENT)
				.paymentType(PaymentType.CREDIT).transactionDescription("Paying house rent").build();

		ApiResponse payment = makePaymentServiceImpl.makePayment(paymentDto);
		assertEquals(ResponseMessages.TRANSACTION_COMPLETED_SUCCESSFULLY_MESSAGE, payment.message());

	}

}
