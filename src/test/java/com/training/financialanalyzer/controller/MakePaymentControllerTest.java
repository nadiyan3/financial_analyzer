package com.training.financialanalyzer.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.financialanalyzer.dto.ApiResponse;
import com.training.financialanalyzer.dto.PaymentDto;
import com.training.financialanalyzer.entity.Category;
import com.training.financialanalyzer.entity.PaymentType;
import com.training.financialanalyzer.service.MakePaymentService;

@ExtendWith(SpringExtension.class)
class MakePaymentControllerTest {
	
	@InjectMocks
	private  MakePaymentController makePaymentController;
	
	@Mock
	private  MakePaymentService makePaymentService;
	
	@Test
	void makePaymentSuccefull() {
		PaymentDto paymentDto = PaymentDto.builder()
				.amount(200D).category(Category.GOLD_LOAN)
				.customerId(1l).paymentType(PaymentType.CREDIT).build();
		ApiResponse api = ApiResponse.builder().httpsCode("201").build();
		Mockito.when(makePaymentService.makePayment(paymentDto)).thenReturn(api);
		ResponseEntity<ApiResponse> payment = makePaymentController.makePayment(paymentDto);
		assertEquals("201", payment.getBody().httpsCode());
	}

}
