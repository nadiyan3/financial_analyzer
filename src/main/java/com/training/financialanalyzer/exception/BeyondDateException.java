package com.training.financialanalyzer.exception;

public class BeyondDateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BeyondDateException(String message) {
		super(message);
	}

}
