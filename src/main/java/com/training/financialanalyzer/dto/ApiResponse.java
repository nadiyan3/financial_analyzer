package com.training.financialanalyzer.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, String httpsCode) {
}
