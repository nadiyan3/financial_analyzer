package com.training.financialanalyzer.dto;

import com.training.financialanalyzer.entity.Category;
import com.training.financialanalyzer.entity.PaymentType;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record PaymentDto(@NotNull(message = "CustomerId is required") Long customerId,
		@NotNull(message = "amount is required") @Min(value = 5,
		message = "minimum amount must be 5") @Max(value = 50000, message = "maximum is 50000") double amount,
		PaymentType paymentType, 
		@NotBlank(message = "Description needed")String transactionDescription, 
		Category category) {

}
