package com.training.financialanalyzer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.financialanalyzer.dto.ApiResponse;
import com.training.financialanalyzer.dto.PaymentDto;
import com.training.financialanalyzer.service.MakePaymentService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * @author praveenakumara.hitne
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class MakePaymentController {
	
	private final MakePaymentService makePaymentService;
	/**
	 *Make payment based on Debit or Credit
	 * @param paymentDto
	 * @return success code and message
	 */
	@PostMapping("/transactions")
	public ResponseEntity<ApiResponse> makePayment(@Valid @RequestBody PaymentDto paymentDto){
		log.info("makePayment add in controller class");
		return new ResponseEntity<>(makePaymentService.makePayment(paymentDto),HttpStatus.CREATED);
	}

}
