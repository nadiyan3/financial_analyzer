package com.training.financialanalyzer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.financialanalyzer.entity.Transaction;


public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	
}
