package com.training.financialanalyzer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.financialanalyzer.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
