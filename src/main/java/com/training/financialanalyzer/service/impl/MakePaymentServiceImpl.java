package com.training.financialanalyzer.service.impl;

import org.springframework.stereotype.Service;

import com.training.financialanalyzer.dto.ApiResponse;
import com.training.financialanalyzer.dto.PaymentDto;
import com.training.financialanalyzer.entity.Customer;
import com.training.financialanalyzer.entity.PaymentType;
import com.training.financialanalyzer.entity.Transaction;
import com.training.financialanalyzer.exception.CustomerNotFoundException;
import com.training.financialanalyzer.exception.InsufficientFundException;
import com.training.financialanalyzer.repository.CustomerRepository;
import com.training.financialanalyzer.repository.TransactionRepository;
import com.training.financialanalyzer.service.MakePaymentService;
import com.training.financialanalyzer.util.ResponseMessages;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class MakePaymentServiceImpl implements MakePaymentService {

	private final TransactionRepository transactionRepository;
	private final CustomerRepository customerRepository;
	
	/**
	 * customer able to make and receive payments.
	 * @param customerId
	 * @return 	 transaction updated
	 */

	@Override
	public ApiResponse makePayment(PaymentDto paymentDto) {

		log.warn("Customer ID not found");
		Customer customer = customerRepository.findById(paymentDto.customerId()).orElseThrow(
				() -> new CustomerNotFoundException(ResponseMessages.CUSTOMER_NOT_FOUND_EXCEPTION_MESSAGE));

		if (PaymentType.DEBIT.equals(paymentDto.paymentType()) && customer.getOpeningBalance() < paymentDto.amount()) {
			log.warn("Insficient fund in customer account");
			throw new InsufficientFundException(ResponseMessages.INSUFFICIENT_FUND_EXCEPTION_MESSAGE);
		} else if (PaymentType.CREDIT.equals(paymentDto.paymentType())) {
			customer.setOpeningBalance(customer.getOpeningBalance() + paymentDto.amount());
			log.info("amount credited customer account");
			customerRepository.save(customer);

		} else if (PaymentType.DEBIT.equals(paymentDto.paymentType())) {
			customer.setOpeningBalance(customer.getOpeningBalance() - paymentDto.amount());
			log.info("amount debited customer account ");
			customerRepository.save(customer);
		}

		Transaction transaction = Transaction.builder().customer(customer).amount(paymentDto.amount())
				.category(paymentDto.category()).paymentType(paymentDto.paymentType())
				.transactionDescription(paymentDto.transactionDescription()).build();
		log.info("Transaction update");
		transactionRepository.save(transaction);

		return ApiResponse.builder().message(ResponseMessages.TRANSACTION_COMPLETED_SUCCESSFULLY_MESSAGE)
				.httpsCode(ResponseMessages.TRANSACTION_COMPLETED_SUCCESSFULLY_CODE).build();
	}

}
