package com.training.financialanalyzer.service;

import com.training.financialanalyzer.dto.ApiResponse;
import com.training.financialanalyzer.dto.PaymentDto;

public interface MakePaymentService {

	ApiResponse makePayment(PaymentDto paymentDto);

}
