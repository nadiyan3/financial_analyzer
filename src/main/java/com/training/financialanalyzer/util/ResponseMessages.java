package com.training.financialanalyzer.util;

public interface ResponseMessages {

	/**
	 * Success Responses
	 * 
	 */
	String TRANSACTION_COMPLETED_SUCCESSFULLY_CODE = "201";
	String TRANSACTION_COMPLETED_SUCCESSFULLY_MESSAGE = "Transaction Completed Successfully";

	/**
	 * Exception Responses
	 */
	
	String CUSTOMER_NOT_FOUND_EXCEPTION_CODE = "FSA0001";
	String CUSTOMER_NOT_FOUND_EXCEPTION_MESSAGE = "Customer not found";
	String INSUFFICIENT_FUND_EXCEPTION_CODE = "FSA0004";
	String INSUFFICIENT_FUND_EXCEPTION_MESSAGE ="Insufficient fund in customer account"; 

}
